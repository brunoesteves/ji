import React from "react";
import "./Footer.css";

const Footer: React.FC = () => {
  return (
    <div className="footer">
      <span>Jogos Indie Direitos Reservados.</span>
    </div>
  );
};

export default Footer;
